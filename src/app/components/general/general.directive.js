(function() {
  'use strict';

  angular
    .module('myNewProject')
    .directive('tree', tree)
    .directive('module', module)
    .directive('courseUnit', courseUnit)
    .factory('util', util)
;

  /** @ngInject */
  function tree(util) {
    return {
      restrict: 'E',
      template: '',
      scope: {
        tree: '='
      },
      link: function(scope, element) {
        for(var i = 0; i < scope.tree.length;++i) {
          var childScope = {};
          var row = scope.tree[i];
          childScope.row = angular.copy(row);
          element.append(util.build(childScope));
        }
      }
    }
  }
  /** @ngInject */
  function module(util) {
    return {
      restrict: 'E',
      template: '<p ng-click="hide()">module: {{row.name}}</p>',
      scope: {
        row: '='
      },
      link: function(scope, element) {
        scope.hide = function() {
          element.toggleClass('hide-child');
        };
        for(var i = 0; i < scope.row.nodes.length;++i) {
          var childScope = {};
          childScope.row = angular.copy(scope.row.nodes[i]);
          element.append(util.build(childScope));
        }
      }
    };
  }

  /** @ngInject */
  function courseUnit() {
    return {
      restrict: 'E',
      template: '<p>courseUnit: {{row.name}}</p>',
      scope: {
        row: '='
      }
    };
  }

  /** @ngInject */
  function util($compile, $rootScope) {
    return {
      build: function(scope) {
        var newScope = $rootScope.$new();
        newScope.row = scope.row;
        if (scope.row.type === 'module') {
          return $compile('<module row="row"></module>')(newScope)
        } else if (scope.row.type === 'courseUnit') {
          return $compile('<course-unit row="row"></course-unit>')(newScope)
        }
      }
    }
  }

})();
