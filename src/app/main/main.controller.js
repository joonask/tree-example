(function() {
  'use strict';

  angular
    .module('myNewProject')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController() {
    var main = this;
    main.tree = [
      {type: 'module', name: 'mod1', nodes: [
        {type: 'courseUnit', name: 'child1'},
        {type: 'courseUnit', name: 'child2'},
        {type: 'module', name: 'mod2', nodes: [
          {type: 'courseUnit', name: 'child3'},
          {type: 'courseUnit', name: 'child4'},
          {type: 'module', name: 'mod3', nodes: [
            {type: 'courseUnit', name: 'child5'},
            {type: 'courseUnit', name: 'child6'},
            {type: 'module', name: 'mod4', nodes: [
              {type: 'courseUnit', name: 'child10'},
              {type: 'courseUnit', name: 'child11'}
            ]}
          ]},
          {type: 'courseUnit', name: 'child7'},
          {type: 'courseUnit', name: 'child8'},
          {type: 'courseUnit', name: 'child9'}
        ]}
      ]}
    ];
  }
})();
